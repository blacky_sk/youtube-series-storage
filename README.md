# [BETA] Youtube series storage #
*Make your youtube life easier*

## About ##
I don't know how about you but I always had problem with managing series on youtube. Different tags, titles, descriptions... So I've decided to create this simple program. It stores your series in file `youtube_generator.txt`.

## Requirements ##
All python requirements can be satisfied with `pip install -r requirements.txt`.

## How to install ##
1. Download and install latest version of python: [Python download](https://www.python.org/ftp/python/2.7.12/python-2.7.12.msi)
2. Download latest version of my script: [Script download](https://bitbucket.org/Sl0v4k14/youtube-series-storage/downloads) - download repository
3. Run script through command line as `python youtube.py`