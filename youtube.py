import Tkinter
import TkinterMessagebox
import tkMessageBox
import json
import base64
import os.path

app_data =  {}

if os.path.isfile("youtube_generator.txt"):
        with open("youtube_generator.txt","r") as save:
            app_data = json.loads(base64.b64decode(save.read()))
            save.close()

while len(app_data.keys()) == 0:
    dialog = TkinterMessagebox.mbox(msg="Enter name of new Game / Series: ", entry=True)
    if dialog == 0:
        exit()
    app_data[dialog] = {"title": "", "description": "", "tags": []}

current_series = None
series_saved = True
child_window = None

def change_series(event):
    if not series_saved:
        answer = tkMessageBox.askyesno(message="Do you really want to change series? This will discard any changes you've made.");
        if not answer:
            choosed_game.set("Choose Game / Series")
            return
    global current_series
    current_series = choosed_game.get()
    video_title.delete('1.0', Tkinter.END)
    video_description.delete('1.0', Tkinter.END)
    video_tags.delete('1.0', Tkinter.END)
    video_title.insert(Tkinter.INSERT, app_data[current_series]["title"])
    video_description.insert(Tkinter.INSERT, app_data[current_series]["description"])
    video_tags.insert(Tkinter.INSERT, ", ".join(app_data[current_series]["tags"]))

def refresh_optionmenu():
    global app_data
    if len(app_data.keys()) == 0:
        while len(app_data.keys()) == 0:
            dialog = TkinterMessagebox.mbox(msg="Enter name of new Game / Series: ", entry=True)
            if dialog == 0:
                exit()
            app_data[dialog] = {"title": "", "description": "", "tags": []}
    series_optionmenu = Tkinter.OptionMenu(choose_game_frame, choosed_game, *app_data.keys(), command=change_series)
    series_optionmenu.config(width=69)
    series_optionmenu.grid(row=0,column=0,sticky="w")

def add_series():
    dialog = TkinterMessagebox.mbox(msg="Enter name of new Game / Series: ", entry=True)
    if dialog == 0:
        return
    if dialog in app_data.keys():
        tkMessageBox.showerror("Error occurred", "Name already exist")
        return
    global app_data
    app_data[dialog] = {"title": "", "description": "", "tags": []}
    refresh_optionmenu()

def rename_series():
    global current_series
    if current_series is None:
        tkMessageBox.showwarning(message="Select series")
        return
    new_name = TkinterMessagebox.mbox(msg="Enter new name of new Game / Series: ", entry=True)
    if new_name == 0:
        return
    if new_name in app_data.keys():
        tkMessageBox.showerror("Error occurred", "Name already exist")
        return
    app_data[new_name] = app_data.pop(current_series)
    current_series = new_name
    refresh_optionmenu()
    choosed_game.set(new_name)

def remove_series():
    if current_series is None:
        tkMessageBox.showwarning(message="Select series")
        return
    confirmed = tkMessageBox.askyesno(message="Do you want to remove '"+current_series+"'?")
    if not confirmed:
        return
    global app_data
    del app_data[current_series]
    global current_series
    current_series = None
    refresh_optionmenu()
    choosed_game.set("Choose Game / Series")
    video_title.delete('1.0', Tkinter.END)
    video_description.delete('1.0', Tkinter.END)
    video_tags.delete('1.0', Tkinter.END)

def save_state():
    with open("youtube_generator.txt","w") as save:
        save.write(base64.b64encode(json.dumps(app_data)))
        save.close()

def process(event):
    global app_data
    if not current_series is None:
        app_data[current_series]["title"] = video_title.get('1.0', Tkinter.END).strip()
        app_data[current_series]["description"] = video_description.get('1.0', Tkinter.END).strip()
        app_data[current_series]["tags"] = []
        for tag in video_tags.get('1.0', Tkinter.END).split(","):
            if len(tag.strip()) != 0:
                app_data[current_series]["tags"].append(tag.strip())

def generate_description():
    final_description = ""
    final_description = final_description + description_header.get("1.0", Tkinter.END).strip() + "\n\n"
    if blacky_check.get():
        final_description = final_description + "Blacky gaming:\nhttps://youtube.com/blacky\n\n"
    if john_check.get():
        final_description = final_description + "John Megacycle:\nhttps://youtube.com/jmegs\n\n"
    if ninja_check.get():
        final_description = final_description + "Ninja squirrel:\nhttps://youtube.com/ninja\n\n"
    if frenkie_check.get():
        final_description = final_description + "Frenkie:\nhttps://youtube.com/frenkie\n\n"
    if megalink_check.get():
        final_description = final_description + "Check out our website:\nhttps://megacycleentertainment.com\n\n"
    final_description = final_description + description_footer.get("1.0", Tkinter.END).strip()
    return final_description

def generate_description_clipboard():
    global child_window
    generate_description()
    child_window.destroy()
    child_window = None

def generate_description_area():
    global child_window
    video_description.delete('1.0', Tkinter.END)
    video_description.insert(Tkinter.INSERT, generate_description())
    child_window.destroy()
    child_window = None


def open_generator():
    global child_window
    if not child_window is None:
        child_window.destroy()
    child_window = Tkinter.Tk()

    w = Tkinter.Label(child_window, text="Description header")
    w.grid(row=0,column=0,padx=10,pady=10,sticky=Tkinter.W)

    global description_header
    description_header = Tkinter.Text(child_window, height=4)
    description_header.grid(row=0,column=1,padx=10,pady=10)
    
    w = Tkinter.Label(child_window, text="Video with")
    w.grid(row=1,column=0,padx=10,pady=10,sticky=Tkinter.W)

    frame = Tkinter.Frame(child_window)
    frame.grid(row=1,column=1)

    global blacky_check
    global john_check
    global ninja_check
    global frenkie_check
    global megalink_check
    blacky_check = Tkinter.BooleanVar(child_window)
    john_check = Tkinter.BooleanVar(child_window)
    ninja_check = Tkinter.BooleanVar(child_window)
    frenkie_check = Tkinter.BooleanVar(child_window)
    megalink_check = Tkinter.BooleanVar(child_window)

    w = Tkinter.Checkbutton(frame,text="Blacky Gaming",variable=blacky_check)
    w.grid(row=0,column=0,padx=10)
    w = Tkinter.Checkbutton(frame,text="John Megacycle", variable=john_check)
    w.grid(row=0,column=1,padx=10)
    w = Tkinter.Checkbutton(frame,text="Ninja Squirrel", variable=ninja_check)
    w.grid(row=0,column=2,padx=10)
    w = Tkinter.Checkbutton(frame,text="Frenkie", variable=frenkie_check)
    w.grid(row=0,column=3,padx=10)
    
    w = Tkinter.Label(child_window, text="Features")
    w.grid(row=2,column=0,padx=10,pady=10,sticky=Tkinter.W)

    frame = Tkinter.Frame(child_window)
    frame.grid(row=2,column=1)

    w = Tkinter.Checkbutton(frame,text="megacycleentertainment.com footer text",variable=megalink_check)
    w.grid(row=0,column=0,padx=10)

    w = Tkinter.Label(child_window, text="Description footer")
    w.grid(row=4,column=0,padx=10,pady=10,sticky=Tkinter.W)

    global description_footer
    description_footer = Tkinter.Text(child_window, height=4)
    description_footer.grid(row=4,column=1,padx=10,pady=10)

    frame = Tkinter.Frame(child_window)
    frame.grid(row=5,column=1)

    w = Tkinter.Button(frame, text="Generate to clipboard",width=20,bd=5,relief=Tkinter.RIDGE,font="Helvetica 15 bold",command=generate_description_clipboard)
    w.grid(row=0,column=0,padx=10,pady=10,sticky="ew")

    w = Tkinter.Button(frame, text="Generate to description area",width=25,bd=5,relief=Tkinter.RIDGE,font="Helvetica 15 bold",command=generate_description_area)
    w.grid(row=1,column=0,padx=10,pady=10,sticky="ew")

####################### GUI #######################
root_window = Tkinter.Tk()
root_window.resizable(width=False, height=False)
root_window.geometry('{}x{}'.format(800, 600))
root_window.bind("<Key>", process)

w = Tkinter.Label(root_window, text="Game / Series")
w.grid(row=0,column=0,padx=10,pady=10,sticky=Tkinter.W)

choose_game_frame = Tkinter.Frame(root_window)
choose_game_frame.grid(row=0,column=1,padx=10,sticky="ew")
choosed_game = Tkinter.StringVar(choose_game_frame)
choosed_game.set("Choose Game / Series")
series_optionmenu = Tkinter.OptionMenu(choose_game_frame, choosed_game, *app_data.keys(), command=change_series)
series_optionmenu.config(width=69)
series_optionmenu.grid(row=0,column=0,sticky="w")
w = Tkinter.Button(choose_game_frame, text="Add",width=6,command=add_series)
w.grid(row=0,column=1,padx=10)
w = Tkinter.Button(choose_game_frame, text="Rename",width=6, command=rename_series)
w.grid(row=0,column=2)
w = Tkinter.Button(choose_game_frame, text="Remove",width=6, command=remove_series)
w.grid(row=0,column=3,padx=10)

w = Tkinter.Label(root_window, text="Video title")
w.grid(row=1,column=0,padx=10,pady=10,sticky=Tkinter.W)

video_title = Tkinter.Text(root_window, height=1)
video_title.grid(row=1,column=1,padx=10,pady=10)

w = Tkinter.Label(root_window, text="Video description")
w.grid(row=2,column=0,padx=10,pady=10,sticky=Tkinter.W)

global video_description
video_description = Tkinter.Text(root_window, height=15)
video_description.grid(row=2,column=1,padx=10)

w = Tkinter.Label(root_window, text="Video tags")
w.grid(row=3,column=0,padx=10,pady=10,sticky=Tkinter.W)

video_tags = Tkinter.Text(root_window, height=5)
video_tags.grid(row=3,column=1,padx=10, pady=10)

w = Tkinter.Label(root_window, text="Description generator")
w.grid(row=4,column=0,padx=10,pady=10,sticky=Tkinter.W)

w = Tkinter.Button(root_window, text="Open generator",command=open_generator)
w.grid(row=4,column=1,padx=20,sticky="ew")

w = Tkinter.Button(root_window, text="Save",width=6,bd=5,relief=Tkinter.RIDGE,font="Helvetica 15 bold",command=save_state)
w.grid(row=5,column=1,padx=20,sticky="ew")
root_window.mainloop()